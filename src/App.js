import logo from './logo.svg';
import './App.css';
import {PDFDocument} from "pdf-lib"
import downloadjs  from "downloadjs"

/*El sigunte código permite procesar el pdf para crear un documento específico por cada página
Queda pendiente
1- Configurar servidor para envio de mails
2- Incoporar librería que permita obtener el contenido de los PDF y de esta forma obtener los nombres de usuario correspondientes*/








function App() {
  async function embedPdfPages() {
    // guardo el PDF en un arrayBuffer
    const pdfUrl = 'assets/nombres.pdf';
  
    const nombresPdfBytes = await fetch(pdfUrl).then((res) => res.arrayBuffer());
    // Cargo el PDF
    const loadedPdf = await PDFDocument.load(nombresPdfBytes);
   // Obtengo el número de páginas de todo el documento
    const numberOfPages=loadedPdf.getPageCount()
    console.log("Estas las paginas del pdf"+ numberOfPages)
    //Por cada pagina creo un documento nuevo y lo guardo
    for (let i=0 ; i <numberOfPages; i++)
    {
    const pdfDoc= await PDFDocument.create();
    const [firstPdfPage]= await pdfDoc.copyPages(loadedPdf, [i])
    pdfDoc.addPage(firstPdfPage)
    const pdfBytes = await pdfDoc.save()
   
  
    
    
    // Descargo un documento distinto por cada pagina
    downloadjs(pdfBytes, "Documento información", "application/pdf");

    /*Pasos siguientes para cumplir los requerimiento solicitado
    Implementar librearias que permitan extraer el contenido de cada PDF como texto
    Congigurar servidor de mails a traves de lirberias que permitan incorporar dicha funcion a react


    */
    
 
  }
  
  }
    




  return (
    <div className="App">
      <header className="App-header">
        <h1>Procesamiento PDF</h1>
        <p>Para probar el procesamiento de PDF haz click en el botón de abajo</p>
       <button onClick={embedPdfPages}>
       descargar pdf
       </button>
      </header>
    </div>
  );
}

export default App;
